# Expliot #
(Pronounced - expl-aa-yo-tee)

Internet Of Things Exploitation Framework
--

Expliot is a framework for security testing IoT and IoT infrastructure. It provides a set of plugins (test cases) 
and can be extended easily to create new plugins. The name expliot is a pun on exploit and explains the purpose of
the framework i.e. IoT exploitation. It is developed in python3

### Objective ###
1. Easy to use
2. Easy to extend
3. Support for most IoT protocols
4. Support for Radio IoT protocols
5. Support for hardware protocols
6. One-stop-shop for IoT and IoT infrastructure security testing.

### Install ###

* Download the repo
* $ cd expliot
* $ python setup.py install

### Run ###
* $ efconsole

### Contribution ###
* Suggest new plugins/test cases
* Share any vulnerability information that can be translated to a plugin
* Please do not submit a patch, instead send me an email about what you have in mind
* Report bugs


